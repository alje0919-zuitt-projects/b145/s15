console.log('Hello World');

let fname = 'John';
let lname = 'Smith';
let age = '30';

function personsAge(fname, lname, age) {
	console.log(fname + ' ' + lname + ' ' + 'is' + ' ' + age + ' ' + 'years of age');
}
personsAge(fname, lname, age);

function returnString() {
	console.log('This was printed inside of the function');
	return true;
}

let isMarried = returnString();
console.log('The value of isMarried is: '+ isMarried);