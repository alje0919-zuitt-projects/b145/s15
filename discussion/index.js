//ways to check if js is successfully link to html
console.log('Hello from JS');
console.error('Error something');
console.warn('Oops Warning!');

//syntax
function errorMessage() {
	console.error('Unexpected Error');
}
errorMessage(); //callout the function

function greetings() { //DECLARATION
	//procedures
	console.log('Salutations from JS!');
}
//INVOCATION
greetings();


//There are different methos in declaring a function in JS
//[SECTION] varilables decalred outside a function can be used INSIDE a function
/* example */
let givenName = 'John';
let familyName = 'Smith';
// lets create a function that will utilize the information outside its scope.
function fullName(){
	//combine the values of the info outside and display it inside the console
	console.log(givenName + ' ' + familyName);
}
//Invoking/calling out functions, we do it by adding parenthesis() after the function name
fullName();

//SECTION 2: BLOCKED SCOPE varilables
	//variables that can be used within the scope of the function.
function computeTotal() {
	let numA = 20;
	let numB = 5;
	//lets add values and display it inside the console
	console.log(numA + numB);
}
computeTotal();

//SECTION 3: FUNCTIONS WITH PARAMETERS
	//Parameter -> acts as a variable or a container that only exists inside a function. a parameter is used to store information that is provided 
	//a.k.a CATCHERS of data/info

//lets create a function that emulate a pokemon battle.
function pokemon(pangalan) {
	//were going to use the parameter declared on this function to be processed and displayed inside the console.
	console.log('I choose you: ' + pangalan);
}
//invocation
pokemon('Pikachu');

//ARGUMENT - ACTUAL value that is provided inside a function for it to work properly.
	//the term argument is used when functions are called/invoked

//SECTION 4: FUNCTIONS WITH MULTIPLE PARAMETERS
function addNumbers(numA, numB, numC, numD, numE) {
	//display the sum inside the console
	console.log(numA + numB + numC + numD + numE);
}
addNumbers(1,2,3,4,5);

//lets create a function that will generate a person's full Name
function createFullName(fName, mName, lName) {
	console.log(lName + ',' + fName + ' ' + mName);
}
createFullName('Juan', 'Dela', 'Cruz');


//SECTION 5: USING VARIABLES AS MAIN ARGUMENTS
	//allows us to utilize code reusability, this helps us to reduce the amount of code that we have to write down.

let attackA = 'Tackle';
let attackB = 'Thunderbolt';
let attackC = 'Quick Attack';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack) {
	console.log(name + ' use ' + attack);
}
pokemonStats(selectedPokemon, attackC);


//SECTION 6: RETURN STATEMENT/expression
	//used to display the output of a function.
	//to identify which will be the final output/result of the function and at the same time determine the end of the function statement.

function returnString() {
	'Hello World'
	return 2 + 1;
}

let functionOutput = returnString();
console.log(functionOutput);

//other scenario
function returnString1() {
	return 'Hello World'
	2 + 1;
}

let functionOutput1 = returnString1();
console.log(functionOutput1);

//another example
function dialog(){
	console.log('Oops! I did it again')
	console.log('Dont you know..')
	return 'Return test msg..';
	console.log('This msg will be ignored..')
}
console.log(dialog());

//SECTION 7: USING FUNCTIONS AS ARGUMENTS
	//some complex functions use other functions as their main arguments to perform more complex results
function argumentSaFunction() {
	console.log('This function was passed as an argument');
}
function invokeFunction(argumentNaFunction, pangalawangFunction) {
	argumentNaFunction();
	pangalawangFunction(selectedPokemon, attackB);
	pangalawangFunction(selectedPokemon, attackC);
}
//
invokeFunction(argumentSaFunction, pokemonStats);

//if you want to see details/information about a function
console.log(invokeFunction);